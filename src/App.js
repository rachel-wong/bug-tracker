import './App.css';
import { CssBaseline } from '@material-ui/core'
import NavBar from './components/NavBar'
import Add from './components/Add'
import BugList from './components/BugList'
import { useState, useEffect } from 'react'
import { createTheme, ThemeProvider } from '@material-ui/core/styles';

function App() {

  let initialState;
  let initialStaff = ["CTO", "Senior Developer", "Senior Test Analyst"]
  // Data persistence: get from localStorage if possible, otherwise use hardcoded data to start
  if (localStorage.getItem("bugs")) {
    initialState = JSON.parse(localStorage.getItem("bugs"))
  } else {
    initialState = [
      { id: 1, description: "CSS is broken", person: "CTO", date: "07/03/2021", priority: 1, completed: true },
      { id: 2, description: "JS is broken", person: "Senior Developer", date: "17/04/2021", priority: 3, completed: false },
      { id: 3, description: "Test needed", person: "Senior Test Analyst", date: "20/05/2021", priority: 2, completed: false }
    ]
  }

  const [bugs, setBugs] = useState(initialState)
  const [darkMode, setDarkMode] = useState(false);
  const [staff, setStaff] = useState(initialStaff)

  const theme = createTheme({
    palette: {
      type: darkMode ? 'dark' : 'light',
    },
  });

  // action to add bug to state
  const addBug = (newBug) => {
    setBugs([...bugs, newBug])
  }

  // action to add new staff member to the drop down list
  const addStaff = (newStaff) => {
    setStaff([newStaff, ...staff])
  }

  // action to remove bug from state
  const removeBug = (id) => {
    setBugs(bugs.filter(bug => bug.id !== id))
  }

  // action to toggle status on state
  const toggleStatus = (id) => {
    setBugs(bugs.map(bug => bug.id === id ? { ...bug, completed: !bug.completed } : bug))
  }

  // Watch for anytime there is a change to bugs, reset the localstorage with a copy of bugs again
  useEffect(() => {
    localStorage.setItem("bugs", JSON.stringify(bugs))
  }, [bugs])

  return (
    <ThemeProvider theme={ theme }>
      <NavBar darkMode={darkMode} setDarkMode={setDarkMode} addStaff={addStaff}/>
      <Add addBug={addBug} staff={ staff}/>
         <BugList
           bugs={bugs}
           removeBug={removeBug}
        toggleStatus={toggleStatus}
        staff={ staff }
      />
      <CssBaseline />
    </ThemeProvider>
  );
}

export default App;
