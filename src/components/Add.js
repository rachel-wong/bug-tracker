import React, { useState} from 'react'
import { Paper, Container, Button, Typography, Grid, TextField, Select, MenuItem, InputLabel, FormControl} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import { v4 as uuidv4 } from 'uuid';

const useStyles = makeStyles((theme) => {
  return {
    root: {
      marginTop: theme.spacing(4),
    },
    paper: {
      padding: theme.spacing(4)
    },
    fieldSpacing: {
      marginTop: theme.spacing(2)
    },
    datePicker: {
      marginTop: theme.spacing(3)
    }
  }
})


const Add = ({ addBug, staff }) => {
  // create component-level state to create a brand new bug object to add
  const [date, setDate] = useState(null)
  const [description, setDescription] = useState("")
  const [person, setPerson] = useState("")
  const [priority, setPriority] = useState(0)

  // on form submit, create new bug object and use the passed down addTask function
  const handleSubmit = (e) => {
    e.preventDefault() // stop auto-resubmitting
    const newBug = {
      id: uuidv4(),
      date,
      description,
      person,
      priority,
      completed: false
    }
    addBug(newBug)
    setDescription("")
    setPerson("")
    setDate(null)
    setPriority(0)
  }

  const classes = useStyles()

  return (
    <Container className={ classes.root }>
      <Paper className={ classes.paper}>
        <Typography variant="h6" component="h1">Register bug</Typography>
        <form onSubmit={ handleSubmit} >
          <Grid container spacing={2}>
            <Grid item xs={6} >
              <TextField
                label="Description"
                variant="outlined"
                fullWidth
                required
                className={classes.fieldSpacing}
                value={ description }
                onChange={ (e) => setDescription(e.target.value)}
              />
              <TextField
                label="Priority"
                type="number"
                variant="outlined"
                fullWidth
                required
                className={classes.fieldSpacing}
                value={priority}
                InputProps={{ inputProps: { min: 0, max: 5 } }}
                onChange={ (e) => setPriority(e.target.value)}
              />
            </Grid>
            <Grid item xs={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils} >
                <KeyboardDatePicker
                  fullWidth
                  className={classes.datePicker}
                  disableToolbar
                  variant="outlined"
                  label="Date Registered"
                  format="dd/MMM/yyyy"
                  value={ date }
                  onChange={(e) => setDate(e.toLocaleDateString("en-US"))}
                >
                </KeyboardDatePicker>
              </MuiPickersUtilsProvider>
              <FormControl
                variant="outlined"
                className={classes.fieldSpacing}
                fullWidth
              >
              <InputLabel id="demo-simple-select-filled-label">Responsible Person</InputLabel>
              <Select
                labelId="demo-simple-select-filled-label"
                  id="demo-simple-select-filled"
                value={person}
                onChange={(e) => setPerson(e.target.value)}
              >
                <MenuItem value="">
                  <em>All</em>
                </MenuItem>
                {staff.length && staff.map((item, key) => (
                  <MenuItem key={ key }value={item}>{ item}</MenuItem>
                ))}
              </Select>
            </FormControl>
            </Grid>
          </Grid>
          <Grid container justifyContent="flex-end" classes={{ root: classes.root }}>
              <Button
              type="submit"
              variant="contained"
              color="primary"
              size="large"
              className={classes.button}
              startIcon={<DoneOutlineIcon />}
            >
              Save
              </Button>
            </Grid>
        </form>
      </Paper>
    </Container>
  )
}

export default Add
