import { Modal as MuiModal, Backdrop, Fade, Button, IconButton, TextField, Grid } from '@material-ui/core'
import React, { useState }from 'react'
import { makeStyles } from '@material-ui/core/styles';
import ControlPointDuplicateIcon from '@material-ui/icons/ControlPointDuplicate';
import ClearIcon from '@material-ui/icons/Clear';
const useStyles = makeStyles((theme) => {
  return {
    rowRoot: {
      fontSize: "1.8rem"
    },
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    }
  }
})

const Modal = ({ open, handleClose, addStaff }) => {

  const classes = useStyles()
  const [role, setRole] = useState("")

  const handleSubmit = (e) => {
    e.preventDefault();
    if (role) {
      addStaff(role)
      setRole("")
      handleClose()
    }
  }
  return (
    <div>
     <MuiModal
        aria-labelledby="Add person"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
        className={ classes.modal}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <Grid container justify="flex-end">
              <IconButton onClick={handleClose}>
                <ClearIcon />
              </IconButton>
            </Grid>
            <form noValidate autoComplete="off" onSubmit={ handleSubmit }>
              <TextField label="Role name" fullWidth value={ role} onChange={ (e) => setRole(e.target.value)} />
              <Button
                variant="contained"
                color="primary"
                size="small"
                className={classes.button}
                startIcon={<ControlPointDuplicateIcon />}
                type="submit"
              >
                Add Person
              </Button>
            </form>
          </div>
        </Fade>
      </MuiModal>
    </div>
  )
}

export default Modal
