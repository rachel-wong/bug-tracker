import React, { useState} from 'react'
import { TableCell, IconButton, TableRow, Switch, Grid} from '@material-ui/core'
import ClearIcon from '@material-ui/icons/Clear';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import LocationSearchingIcon from '@material-ui/icons/LocationSearching';


const useStyles = makeStyles((theme) => {
  return {
    rowRoot: {
      fontSize: "1.8rem"
    },
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',

      "& .makeStyles-paper-13": {
        width: '50%'
      }
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    }
  }
})


const BugEntry = ({ bug, toggleStatus, removeBug }) => {

  const { description, priority, date, person, id, completed } = bug
  const classes = useStyles()
  const theme = useTheme();
  const [open, setOpen] = useState(false)

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
          <h2 id="transition-modal-title">{ description}</h2>
          <p id="transition-modal-description">Lodged by {person} on { date }</p>
          </div>
        </Fade>
      </Modal>
    <TableRow className={classes.rowRoot }
      style={completed ? { background: theme.palette.primary.light } : { background: theme.palette.secondary.light }}>
      <TableCell component="th" scope="row">
        <IconButton aria-label="delete" onClick={ () => removeBug(id) }>
          <ClearIcon fontSize="large" />
        </IconButton>
      </TableCell>
      <TableCell component="th" scope="row">
        <Grid container >
          <Grid item xs={ 10}>
            {description}
          </Grid>
          <Grid item container xs={2}>
          <IconButton aria-label="expand for details" onClick={ handleOpen}>
          <LocationSearchingIcon fontSize="large" />
        </IconButton>
          </Grid>
        </Grid>
      </TableCell>
      <TableCell align="right">{ priority  }</TableCell>
      <TableCell align="right">{ date }</TableCell>
      <TableCell align="right">{ person}</TableCell>
      <TableCell align="right">
        <Switch
          onChange={() => toggleStatus(id)}
          color="primary"
          name="Status"
          checked={ completed }
        />
      </TableCell>
      </TableRow>
    </>
  )
}

export default BugEntry
