import React from 'react'
import BugEntry from './BugEntry'
import { Container, Table, Box, Typography, TableContainer, TableHead, TableRow, TableCell, TableBody, Paper } from '@material-ui/core'
import { makeStyles} from '@material-ui/styles'

const useStyles = makeStyles((theme) => {
  return {
    container: {
      marginTop: '2rem',
      marginBottom: '2rem'
    },
    errorMessage: {
      padding: '4rem'
    }
  }
})
const BugList = ({ bugs, toggleStatus, removeBug, staff }) => {

  const classes = useStyles()

  return (
    <Container className={classes.container}>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align="right"></TableCell>
              <TableCell>Description</TableCell>
              <TableCell align="right">Priority</TableCell>
              <TableCell align="right">Date</TableCell>
              <TableCell align="right">Person</TableCell>
              <TableCell align="right">Closed?</TableCell>
            </TableRow>
          </TableHead>
            {bugs.length ?
              <TableBody>
                {bugs.map((bug, idx) => (
                  <BugEntry bug={bug} key={idx} toggleStatus={toggleStatus} removeBug={removeBug} />
                ))}</TableBody> :
              <Box className={ classes.errorMessage}>
                <Typography variant="h5" component="h6" align="center">No bugs yet to register. Add some!</Typography>
              </Box>}
        </Table>
      </TableContainer>
    </Container>
  )
}

export default BugList
