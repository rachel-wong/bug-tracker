import { AppBar, Button, Toolbar, IconButton, Typography, Container, Grid } from '@material-ui/core'
import React, { useState } from 'react'
import WbSunnyIcon from '@material-ui/icons/WbSunny';
import BugReportIcon from '@material-ui/icons/BugReport';
import NightsStayOutlinedIcon from '@material-ui/icons/NightsStayOutlined';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import Modal from './Modal'

const NavBar = ({ darkMode, setDarkMode, addStaff }) => {
  const [openModal, setOpenModal] = useState(false)

  const handleOpen = () => {
    setOpenModal(true);
  };
  const handleClose = () => {
    setOpenModal(false);
  };

  return (
    <>
    <Modal open={openModal} handleClose={handleClose} addStaff={ addStaff }/>
    <AppBar position="static" color="primary">
      <Toolbar>
        <Container>
          <Grid container>
            <Grid container item xs={6} alignItems="center">
              <BugReportIcon />
              <Typography variant="h6">BugTracker</Typography>
          </Grid>
            <Grid container item xs={6} justifyContent="flex-end" alignItems="center">
              <Button
                onClick={handleOpen}
                variant="contained"
                color="primary"
                size="large"
                disableElevation
                startIcon={<AssignmentIndIcon />}
              >
                Add Person
              </Button>
              <IconButton
                edge="start"
                color="inherit"
                aria-label="dark mode trigger"
                onClick={() => setDarkMode(!darkMode)}
              >
                { darkMode ? <WbSunnyIcon/> : <NightsStayOutlinedIcon /> }
                </IconButton>
            </Grid>
            </Grid>
        </Container>
      </Toolbar>
      </AppBar>
    </>
  )
}

export default NavBar
