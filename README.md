## BugTracker

https://bugtracking.netlify.app/

![screenshot](/src/images/screenshot.gif)

#### Functional Requirements

* :white_check_mark: Add tickets for specified bug with priority, assigned person (from a list), description, date registered
* :negative_squared_cross_mark: Sort table by priority, date registered
* :negative_squared_cross_mark: Paginate table
* :negative_squared_cross_mark: Add more persons to assigned person fixed list
* :white_check_mark: MaterialUI